/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@interface SAAlertConfig : NSObject

@property (strong, nonatomic, readwrite) UIColor *alertViewIndicatorColor;
@property (strong, nonatomic, readwrite) NSString *alertViewSuccessImage;
@property (strong, nonatomic, readwrite) NSString *alertViewWarningImage;
@property (strong, nonatomic, readwrite) NSString *alertViewErrorImage;
@property (strong, nonatomic, readwrite) NSString *alertViewTitleFontFamily;
@property (strong, nonatomic, readwrite) NSString *alertViewBodyFontFamily;
@property (strong, nonatomic, readwrite) NSString *alertViewButtonsFontFamily;
@property (nonatomic, assign) CGFloat alertViewTitleFontSize;
@property (nonatomic, assign) CGFloat alertViewBodyFontSize;
@property (nonatomic, assign) CGFloat alertViewButtonsFontSize;
@property (nonatomic, assign) CGFloat alertViewButtonsCornerRadius;

+ (SAAlertConfig *)createAlertConfig;

@end
