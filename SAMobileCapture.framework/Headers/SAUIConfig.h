/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@interface SAUIConfig : NSObject

@property (strong, nonatomic, readwrite) NSString *customStatusBarImage;
@property (strong, nonatomic, readwrite) NSString *customViewImage;
@property (strong, nonatomic, readwrite) NSString *customBottomBGImageForX;
@property (strong, nonatomic, readwrite) UIColor *statusBarColor;
@property (strong, nonatomic, readwrite) UIColor *navBarColor;
@property (strong, nonatomic, readwrite) UIColor *viewColor;
@property (strong, nonatomic, readwrite) UIColor *toolBarColor;
@property (strong, nonatomic, readwrite) UIColor *bottomBGColorForX;
@property (strong, nonatomic, readwrite) NSString *lightFont;
@property (strong, nonatomic, readwrite) NSString *regularFont;
@property (strong, nonatomic, readwrite) NSString *alertButtonFont;
@property (strong, nonatomic, readwrite) NSString *boldFont;
@property (nonatomic, assign) BOOL barTranslucent;
@property (nonatomic, assign) CGFloat navBarTitleFontSize;
@property (nonatomic, assign) CGFloat barButtonItemTitleFontSize;
@property (nonatomic, assign) CGFloat alertButtonFontSize;
@property (nonatomic, assign) CGFloat alertTittleTop;
@property (nonatomic, assign) CGFloat alertDetailTop;
@property (nonatomic, assign) CGFloat alertButtonTop;
@property (nonatomic, assign) CGFloat alertButtonBottom;
@property (strong, nonatomic, readwrite) UIColor *successColor;
@property (strong, nonatomic, readwrite) UIColor *noticeColor;
@property (strong, nonatomic, readwrite) UIColor *warningColor;
@property (strong, nonatomic, readwrite) UIColor *errorColor;
@property (strong, nonatomic, readwrite) UIImage *pdfRotateIcon;
@property (strong, nonatomic, readwrite) UIImage *infoIcon;

+ (SAUIConfig *)createUIConfig;

@end
